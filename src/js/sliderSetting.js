let windowWidth;

function bxSlideCount(count, margin) {
	$('.bxslider').bxSlider({
		nextSelector: '#next-slide',
		prevSelector: '#prev-slide',
		minSlides: count,
		maxSlides: count,
		moveSlides: 2,
		maxHeight: 300,
		adaptiveHeight: true,
		slideWidth: 5000,
		slideMargin: margin,
		infiniteLoop: true
	});
};

function bxSliderSetting() {
	windowWidth = $(window).width();

	if (windowWidth > 1200) {
		bxSlideCount(5, 25);
	} else {
		bxSlideCount(2, 10);
	}
};

bxSliderSetting();