const video = $('#video');
const darken = $('#darken-video');
const playBtn = $('#play-video');

playBtn.click(function () {
	video[0].play();
  playBtn.addClass('hide');
	darken.addClass('hide');
});

video.click(function () {
	video[0].pause();
  playBtn.removeClass('hide');
	darken.removeClass('hide');
});