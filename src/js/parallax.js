$(document).ready(function() {
	const header = $('.header-top');
	const menu = $('nav.header');
	const footer = $('footer.header');
	const topButton = $('.btn-line');
	const content = $('.content');
	const darkenParallax = $('.header-top .darken-parallax');
	const scrollDown = $('.scroll-down');

	const body = $('html body');

	let	windowHeight;

	function headerHeight() {
		if (body.hasClass('fullscreen-header')) {
			windowHeight = $(window).height();
		} else {
			windowHeight = ($(window).height() / 2);
		};
		if (header) {
			header.css({
				'height': windowHeight
			});
			content.css({
				'top': windowHeight
			});
		};
	};

	function headerParallax() {
		let scrollTop = $(window).scrollTop();
		let opacityBG = scrollTop / (windowHeight * 0.95);

		header.css({
			'top': -$(window).scrollTop()/5
		});
		darkenParallax.css({
			'opacity': opacityBG
		});
		if (footer) {
			let opacityFooter = 1 - (scrollTop / (windowHeight / 4));
			footer.css({
				'opacity': opacityFooter
			});

			if (opacityFooter <= 0) {
				footer.css({
					'pointerEvents': 'none'
				});
			} else {
				footer.css({
					'pointerEvents': 'auto'
				});
			};

			if (opacityFooter < 1) {
				footer.addClass('scroll');
			} else {
				footer.removeClass('scroll');
			}
		};

		if (scrollTop > windowHeight * 0.75) {
			menu.addClass('scroll');
			topButton.addClass('scroll');
		} else {
			menu.removeClass('scroll');
			topButton.removeClass('scroll');
		}
	};

	headerHeight();
	headerParallax();

	$(window).resize(function() {
		headerHeight();
	})

	$(document).scroll(function(){
		headerParallax();
	});
});