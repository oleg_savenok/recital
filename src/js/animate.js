//Animate CSS + WayPoints javaScript Plugin
//Example: $(".element").animated("zoomInUp");

(function($) {
	$.fn.animated = function(inEffect) {
		$(this).each(function() {
			var ths = $(this);
			ths.css("opacity", "0").addClass("animated").waypoint(function(dir) {
				if (dir === "down") {
					ths.addClass(inEffect).css("opacity", "1");
				};
			}, {
				offset: "90%"
			});

		});
	};

	$(".first-paragraph").animated("fadeInLeft", "fadeOut");
	$(".second-paragraph").animated("fadeInRight", "fadeOut");
	$("a.news").animated("fadeInUp", "fadeOut");
	$("article").animated("fadeInUp", "fadeOut");
	$(".photos a").animated("fadeInUp", "fadeOut");

})(jQuery);