function initMap() {
	var styleArray = [
		{
			"stylers": [
				{
					"hue": "#ff1a00"
				},
				{
					"invert_lightness": true
				},
				{
					"saturation": -100
				},
				{
					"lightness": 33
				},
				{
					"gamma": 0.5
				}
			]
		},
		{
			"featureType": "water",
			"elementType": "geometry",
			"stylers": [
				{
					"color": "#2D333C"
				}
			]
		}
	];

	var map = new google.maps.Map(document.getElementById('map'), {
		center: {
			lat: 50.404576,
      lng: 30.341886
		},
		zoom: 15,
		styles: styleArray,
		disableDefaultUI: true,
    zoomControl: true,
    zoomControlOptions: {
      position: google.maps.ControlPosition.LEFT_BOTTOM
    }
	});

	// Marker variables
  var iconHome = '../assets/img/markers/index.png';
	var iconBfresh = '../assets/img/markers/shop.png';
	var iconLotok = '../assets/img/markers/market.png';
	var iconSchool = '../assets/img/markers/school.png';
	var iconPharmacy = '../assets/img/markers/hospital.png';
	var iconPizza = '../assets/img/markers/pizza.png';
	var iconSalon = '../assets/img/markers/salon.png';
	var iconCarwash = '../assets/img/markers/carwash.png';

  // Marker content
  var contentHome = '<h5>ЖК "Recital"</h5>';
	var contentBfresh = '<h5>Продуктовий <br> магазин "Bfresh"</h5>';
	var contentLotok = '<h5>Продуктовий <br> магазин "ЛотОК"</h5>';
	var contentSchool = '<h5>Новософіївська <br> школа</h5>';
	var contentPharmacy = '<h5>Аптека <br> "НовоФарм"</h5>';
	var contentPizza = '<h5>Піцерія <br> "IL Pino Pizzeria"</h5>';
	var contentSalon = '<h5>Салон краси <br> "Віктор І Я"</h5>';
	var contentCarwash = '<h5>Автомийка <br> "L-Мойка"</h5>';

  var infowindow = new google.maps.InfoWindow({});

  // Marker position

  // Home
  var markerHome = new google.maps.Marker({
    position: {
      lat: 50.401210,
      lng: 30.344304
    },
    map: map,
    icon: iconHome
  });

  markerHome.addListener('mouseover', function () {
    infowindow.setContent(contentHome);
    infowindow.open(map, this);
    this.setAnimation(google.maps.Animation.BOUNCE);
  });
  markerHome.addListener('mouseout', function () {
    infowindow.close();
    this.setAnimation(null);
  });

	// Home
  var markerBfresh = new google.maps.Marker({
    position: {
      lat: 50.406086,
      lng: 30.340264
    },
    map: map,
    icon: iconBfresh
  });
  markerBfresh.addListener('mouseover', function () {
    infowindow.setContent(contentBfresh);
    infowindow.open(map, this);
    this.setAnimation(google.maps.Animation.BOUNCE);
  });
  markerBfresh.addListener('mouseout', function () {
    infowindow.close();
    this.setAnimation(null);
  });

	// Home
  var markerLotok = new google.maps.Marker({
    position: {
      lat: 50.406418,
      lng: 30.340852
    },
    map: map,
    icon: iconLotok
  });
  markerLotok.addListener('mouseover', function () {
    infowindow.setContent(contentLotok);
    infowindow.open(map, this);
    this.setAnimation(google.maps.Animation.BOUNCE);
  });
  markerLotok.addListener('mouseout', function () {
    infowindow.close();
    this.setAnimation(null);
  });

	// Home
  var markerSchool = new google.maps.Marker({
    position: {
      lat: 50.406426,
      lng: 30.340922
    },
    map: map,
    icon: iconSchool
  });
  markerSchool.addListener('mouseover', function () {
    infowindow.setContent(contentSchool);
    infowindow.open(map, this);
    this.setAnimation(google.maps.Animation.BOUNCE);
  });
  markerSchool.addListener('mouseout', function () {
    infowindow.close();
    this.setAnimation(null);
  });

	// Home
  var MarkerPharmacy = new google.maps.Marker({
    position: {
      lat: 50.406162,
      lng: 30.340099
    },
    map: map,
    icon: iconPharmacy
  });
  MarkerPharmacy.addListener('mouseover', function () {
    infowindow.setContent(contentPharmacy);
    infowindow.open(map, this);
    this.setAnimation(google.maps.Animation.BOUNCE);
  });
  MarkerPharmacy.addListener('mouseout', function () {
    infowindow.close();
    this.setAnimation(null);
  });

	// Home
  var MarkerPizza = new google.maps.Marker({
    position: {
      lat: 50.406354,
      lng: 30.340486
    },
    map: map,
    icon: iconPizza
  });
  MarkerPizza.addListener('mouseover', function () {
    infowindow.setContent(contentPizza);
    infowindow.open(map, this);
    this.setAnimation(google.maps.Animation.BOUNCE);
  });
  MarkerPizza.addListener('mouseout', function () {
    infowindow.close();
    this.setAnimation(null);
  });

	// Home
  var MarkerSalon = new google.maps.Marker({
    position: {
      lat: 50.405482,
      lng: 30.337098
    },
    map: map,
    icon: iconSalon
  });
  MarkerSalon.addListener('mouseover', function () {
    infowindow.setContent(contentSalon);
    infowindow.open(map, this);
    this.setAnimation(google.maps.Animation.BOUNCE);
  });
  MarkerSalon.addListener('mouseout', function () {
    infowindow.close();
    this.setAnimation(null);
  });

	// Home
  var MarkerCarwash = new google.maps.Marker({
    position: {
      lat: 50.404827,
      lng: 30.336798
    },
    map: map,
    icon: iconCarwash
  });
  MarkerCarwash.addListener('mouseover', function () {
    infowindow.setContent(contentCarwash);
    infowindow.open(map, this);
    this.setAnimation(google.maps.Animation.BOUNCE);
  });
  MarkerCarwash.addListener('mouseout', function () {
    infowindow.close();
    this.setAnimation(null);
  });
}

window.initMap = initMap;