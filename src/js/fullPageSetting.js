$(document).ready(function() {
	$('#fullpage').fullpage({
		anchors:['header', 'location', 'infrastructure', 'developer', 'music'],
		navigation: false,
		scrollingSpeed: 800,
		easingcss3: 'cubic-bezier(.4,0,.6,1)',

		onLeave (index, nextIndex, direction) {
			const scrollDown = $('footer .scroll-down');

			function slideNavActive(target) {
				const active = $(`ul.slide-nav a:eq(${target})`);
				const prev = $(`ul.slide-nav a:lt(${target})`);
				const next = $(`ul.slide-nav a:gt(${target})`);

				active.addClass('active');
				prev.removeClass('active');
				next.removeClass('active');
			};

			if (nextIndex == 1) {
				slideNavActive(0);
				scrollDown.removeClass('hide');
      }
			else if (nextIndex == 2) {
        slideNavActive(1);
				scrollDown.removeClass('hide');
      }
			else if (nextIndex == 3) {
      	slideNavActive(2);
				scrollDown.removeClass('hide');
      }
			else if (nextIndex == 4) {
      	slideNavActive(3);
				scrollDown.removeClass('hide');
      }
			else if (nextIndex == 5) {
      	slideNavActive(4);
				scrollDown.addClass('hide');
      }
		}
	});

	$('.scroll-down').click(function() {
		$.fn.fullpage.moveSectionDown();
	});
});