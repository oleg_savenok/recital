$(document).ready(function() {
	const menu = $('nav.header');
	const topButton = $('.btn-line');

	function headerScrollTop(point) {
		let scrollTop = $(window).scrollTop();
		let windowHeight = $(window).height();

		if (scrollTop > windowHeight * point) {
			menu.addClass('scroll');
			topButton.addClass('scroll');
		} else {
			menu.removeClass('scroll');
			topButton.removeClass('scroll');
		}
	};

	headerScrollTop(0.01);

	$(document).scroll(function(){
		headerScrollTop(0.01);
	});
});
