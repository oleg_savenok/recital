$(document).ready(function() {
  const darkenHeader = $('.darken-nav');
  const newsButton = $('nav.navigation ul li.news');
  const newsLink = $('.news-link');
  const hamburger = $('.hamburger');
  const nav = $('nav.nav');
  const footer = $('footer.header');
  const scrollDown = $('.scroll-down');

  const navigation = $('nav nav.navigation');



  const dayButton = $('.day-night .day');
  const nightButton = $('.day-night .night');

  const dayBG = $('.header .day-bg');

  const certificateButton = $('.developer .right-icon .btn-rectangle');
  const certificateGallery = $('.slide-container');
  const certificateClose = $('.slide-container .close-btn');

  let loader = $('#preloader');

	loader.addClass('hide');

  let keys = {37: 1, 38: 1, 39: 1, 40: 1};

  // Animation function for nav elements

  function targetAnimation( target, action, classname ) {
    if (action == 'open') {
      target.addClass(classname);
    } else
    if (action == 'close') {
      target.removeClass(classname);
    }
  };

  function preventDefault(e) {
    e = e || window.event;
    if (e.preventDefault)
      e.preventDefault();
      e.returnValue = false;
  }

  function preventDefaultForScrollKeys(e) {
    if (keys[e.keyCode]) {
      preventDefault(e);
      return false;
    }
  }

  function disableScroll() {
    if (window.addEventListener) // older FF
      window.addEventListener('DOMMouseScroll', preventDefault, false);
      window.onwheel = preventDefault; // modern standard
      window.onmousewheel = document.onmousewheel = preventDefault; // older browsers, IE
      window.ontouchmove  = preventDefault; // mobile
      document.onkeydown  = preventDefaultForScrollKeys;
  }

  function enableScroll() {
    if (window.removeEventListener)
      window.removeEventListener('DOMMouseScroll', preventDefault, false);
      window.onmousewheel = document.onmousewheel = null;
      window.onwheel = null;
      window.ontouchmove = null;
      document.onkeydown = null;
  }

  // Function for disable or enable scroll slides

  function scrollingParameters(param) {
    if (param == 'enable') {
      if ($.fn.fullpage) {
        $.fn.fullpage.setMouseWheelScrolling(true);
        $.fn.fullpage.setAllowScrolling(true);
        $.fn.fullpage.setKeyboardScrolling(true);
      }
    } else if (param == 'disable') {
      if ($.fn.fullpage) {
        $.fn.fullpage.setMouseWheelScrolling(false);
        $.fn.fullpage.setAllowScrolling(false);
        $.fn.fullpage.setKeyboardScrolling(false);
      }
    }
  }

  function navAnimation(action) {
    targetAnimation( nav, action, 'open' );
    targetAnimation( footer, action, 'open' );
    targetAnimation( darkenHeader, action, 'open' );
  }

  function newsAnimation(action) {
    targetAnimation( newsLink, action, 'open-news' );
    targetAnimation( scrollDown, action, 'open-news' );
    targetAnimation( nav, action, 'open-news' );
    targetAnimation( footer, action, 'open-news' );
  }

  // Click on hamburger

  hamburger.click(function() {
    if (nav.hasClass('open')) {
      navAnimation('close');
      newsAnimation('close')
      scrollingParameters('enable');
      enableScroll();
    } else {
      navAnimation('open');
      scrollingParameters('disable')
      disableScroll();
    }
  });

  newsButton.click(function() {
    if (newsLink.hasClass('open-news')) {
      newsAnimation('close')
    } else {
      newsAnimation('open')
    }
  });

  certificateButton.click(function() {
    certificateGallery.addClass('open');
    darkenHeader.addClass('open-slider');
    scrollDown.addClass('open');
    scrollingParameters('disable');
  });

  certificateClose.click(function() {
    certificateGallery.removeClass('open');
    darkenHeader.removeClass('open-slider');
    scrollDown.removeClass('open');
    scrollingParameters('enable');
  });

  dayButton.click(function() {
    dayButton.addClass('active');
    nightButton.removeClass('active');
    dayBG.removeClass('hide');
  });

  nightButton.click(function() {
    nightButton.addClass('active');
    dayButton.removeClass('active');
    dayBG.addClass('hide');
  });

  $(document).click(function (event) {
    if (nav.hasClass('open') && !nav.hasClass('open-news')) {
      if ($(event.target).closest(navigation).length === 0 && $(event.target).closest(hamburger).length === 0) {
        navAnimation('close');
        scrollingParameters('enable');
        enableScroll();
      }
    }
  });
});