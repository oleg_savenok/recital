$('.item').magnificPopup({
	type: 'image',
	gallery: {
		enabled: true
	},
	removalDelay: 500,
	mainClass: 'mfp-fade'
});