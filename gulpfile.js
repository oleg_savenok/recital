var gulp = require('gulp'),
    sass = require('gulp-sass'),
    jade = require('gulp-jade'),
    browserSync = require('browser-sync'),
    autoprefixer = require('gulp-autoprefixer'),
    uglify = require('gulp-uglify'),
    rename = require('gulp-rename'),
    cssnano = require('gulp-cssnano'),
    sourcemaps = require('gulp-sourcemaps'),
    browserify = require('gulp-browserify'),
    clean = require('gulp-clean');

gulp.task('clean-all', function () {
  return gulp.src('public/**/*', {read: false})
    .pipe(clean());
});

gulp.task('clean-js', function () {
  return gulp.src('public/js/*.js', {read: false})
    .pipe(clean());
});

gulp.task('clean-css', function () {
  return gulp.src('public/css/*', {read: false})
    .pipe(clean());
});

gulp.task('clean-pages', function () {
  return gulp.src('public/pages/*', {read: false})
    .pipe(clean());
});

gulp.task('clean-assets', function () {
  return gulp.src('public/assets/**/*', {read: false})
    .pipe(clean());
});

gulp.task('jade', function() {
  gulp.src('src/pages/*.jade')
    .pipe(jade({
      locals: {}
    }))
    .on('error', function(err) {
      console.log(err);
    })
    .pipe(gulp.dest('public'))
});

gulp.task('css', function () {
  return gulp.src('src/styles/main.scss')
    .pipe(sourcemaps.init())
    .pipe(sass().on('error', sass.logError))
    .pipe(autoprefixer('last 3 version'))
    .pipe(gulp.dest('public/css'))
    // .pipe(cssnano())
    // .pipe(rename({suffix: '.min'}))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest('public/css'))
    .pipe(browserSync.reload({stream: true}));
});

gulp.task('css-min', function () {
  return gulp.src('src/styles/main.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(autoprefixer('last 3 version'))
    .pipe(cssnano({
      zindex: false,
      reduceIdents: false
    }))
    .pipe(rename({suffix: '.min'}))
    .pipe(gulp.dest('public/css'))
});

gulp.task('preloader', function () {
  return gulp.src('src/styles/preloader.scss')
    .pipe(sourcemaps.init())
    .pipe(sass().on('error', sass.logError))
    .pipe(autoprefixer('last 3 version'))
    .pipe(gulp.dest('public/css'))
    // .pipe(cssnano())
    // .pipe(rename({suffix: '.min'}))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest('public/css'))
    .pipe(browserSync.reload({stream: true}));
});

gulp.task('preloader-min', function () {
  return gulp.src('src/styles/preloader.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(autoprefixer('last 3 version'))
    .pipe(cssnano({
      zindex: false,
      reduceIdents: false
    }))
    .pipe(rename({suffix: '.min'}))
    .pipe(gulp.dest('public/css'))
});

gulp.task('comming-soon', function () {
  return gulp.src('src/styles/comming-soon.scss')
    .pipe(sourcemaps.init())
    .pipe(sass().on('error', sass.logError))
    .pipe(autoprefixer('last 3 version'))
    .pipe(gulp.dest('public/css'))
    // .pipe(cssnano())
    // .pipe(rename({suffix: '.min'}))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest('public/css'))
    .pipe(browserSync.reload({stream: true}));
});

gulp.task('js', function () {
  gulp.src('src/js/**.js')
    .pipe(sourcemaps.init())
    .pipe(browserify({
      insertGlobals : true,
      debug : true
    }))
    .pipe(gulp.dest('public/js'))
    // .pipe(uglify())
    // .pipe(rename({suffix: '.min'}))
    .pipe(gulp.dest('public/js'))
});

gulp.task('js-min', function () {
  gulp.src('src/js/**.js')
    .pipe(browserify({
      insertGlobals : true,
      debug : true
    }))
    .pipe(uglify())
    .pipe(rename({suffix: '.min'}))
    .pipe(gulp.dest('public/js'))
});

gulp.task('assets', function () {
  gulp.src(['src/assets/**/*'])
    .pipe(gulp.dest('public/assets'));
})

gulp.task('php', function () {
  gulp.src(['src/**/*.php'])
    .pipe(gulp.dest('public'));
})

gulp.task('browser-sync', function () {
  browserSync.init(null, {
    server: {
      baseDir: "public"
    }
  });
});
gulp.task('bs-reload', function () {
  browserSync.reload();
});

gulp.task('server', ['assets', 'php', 'jade', 'css', 'preloader', 'js', 'browser-sync'], function () {
  gulp.watch("src/pages/**/*.jade", ['jade', 'bs-reload']);
  gulp.watch("src/styles/*/*.scss", ['css']);
  gulp.watch("src/styles/preloader.scss", ['preloader']);
  //gulp.watch("src/styles/comming-soon.scss", ['comming-soon']);
  gulp.watch("src/js/*.js", ['js']);
  gulp.watch("src/assets/img/sprites/*", ['sprites']);
  gulp.watch("src/assets/**/*", ['assets', 'bs-reload']);
});

gulp.task('build', ['assets', 'php', 'jade', 'css-min', 'preloader-min', 'js-min']);
